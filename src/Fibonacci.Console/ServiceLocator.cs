﻿using System;
using System.Collections.Generic;
using System.Text;
using Fibonacci.Core.Helpers;

namespace Fibonacci.ConsoleApp
{
    public class ServiceLocator
    {
        private Dictionary<Type, object> _services = new Dictionary<Type, object>();

        private Dictionary<ValueTuple<string,Type>, object> _namedServices = new Dictionary<ValueTuple<string, Type>, object>();

        private ServiceLocator()
        {
            _services.Add(typeof(ISeriesCache), new SeriesCache());

            _namedServices.Add(("Fibonacci", typeof(ISeriesCalculator)), new FibonacciCalculator(Get<ISeriesCache>()));

            _namedServices.Add(("Exponential", typeof(ISeriesCalculator)), new ExponentialCalculator());
        }

        public static ServiceLocator Instace { get; } = new ServiceLocator();

        public TService Get<TService>() where TService:class
        {
            if (_services.ContainsKey(typeof(TService)))
            {
                return _services[typeof(TService)] as TService;
            }

            throw new InvalidOperationException($"{typeof(TService).Name} is not a registered type");
        }

        public TService Get<TService>(string name) where TService : class
        {
            if (_namedServices.ContainsKey((name, typeof(TService))))
            {
                return _namedServices[(name, typeof(TService))] as TService;
            }

            return Get<TService>();
        }
    }
}
