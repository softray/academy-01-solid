﻿using System;
using Fibonacci.Core.Helpers;

namespace Fibonacci.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please select a series");
            Console.WriteLine("\t 1. Fibonacci");
            Console.WriteLine("\t 2. Exponential");

            var typeOfCalculator = Console.ReadLine();

            if (!int.TryParse(typeOfCalculator, out var type))
            {
                return;
            }

            ISeriesCalculator calculator =
                ServiceLocator.Instace.Get<ISeriesCalculator>(type == 1 ? "Fibonacci" : "Exponential");

            while (true)
            {
                Console.WriteLine("Please enter a number");

                var ordinalKey = Console.ReadLine();

                if (!int.TryParse(ordinalKey, out var ordinal))
                {
                    break;
                }

                try
                {
                    Console.WriteLine($"{ordinal}th series item is {calculator.Calculate(ordinal)}");

                    if (calculator is ICachingEntity cacheEntity)
                    {
                        Console.WriteLine($"(Current Number of cached items: {cacheEntity.GetCachedItemsCount()})");
                    }

                    Console.WriteLine();
                }
                catch (ArgumentException ex)
                {
                    Console.Write(ex.Message);
                }
            }
        }
    }
}
