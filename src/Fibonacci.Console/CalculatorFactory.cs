﻿using System;
using System.Collections.Generic;
using System.Text;
using Fibonacci.Core.Helpers;

namespace Fibonacci.ConsoleApp
{
    public class CalculatorFactory
    {
        public ISeriesCalculator GetSeriesCalculator(int type)
        {
            if (type == 1)
            {
                return new FibonacciCalculator(new SeriesCache());
            }
            else
            {
                return new ExponentialCalculator();
            }
        }
    }
}
