﻿namespace Fibonacci.Core.Helpers
{
    public interface ISeriesCache
    {
        int GetNumberOfCachedItems();

        void CacheItem(int ordinal, int value);

        bool TryGetCacheItem(int ordinal, out int result);
    }
}