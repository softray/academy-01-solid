﻿namespace Fibonacci.Core.Helpers
{
    public class ExponentialCalculator : NonCachingCalculator
    {
        public ExponentialCalculator() : base()
        {
        }

        protected override int CalculateOrdinal(int ordinal)
        {
            var squareValue = ordinal * ordinal;

            return squareValue;
        }
    }
}