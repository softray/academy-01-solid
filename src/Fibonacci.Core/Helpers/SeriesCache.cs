﻿using System.Collections.Generic;

namespace Fibonacci.Core.Helpers
{
    public class SeriesCache : ISeriesCache
    {
        private Dictionary<int, int> _cache = new Dictionary<int, int>();

        public int GetNumberOfCachedItems()
        {
            return _cache.Count;
        }

        public void CacheItem(int ordinal, int value)
        {
            _cache.Add(ordinal, value);
        }

        public bool TryGetCacheItem(int ordinal, out int result)
        {
            if (_cache.ContainsKey(ordinal))
            {
                result = _cache[ordinal];
                return true;
            }

            result = -1;

            return false;
        }
    }
}