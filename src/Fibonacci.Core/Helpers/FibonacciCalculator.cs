﻿namespace Fibonacci.Core.Helpers
{
    public class FibonacciCalculator : SeriesCalculator
    {
        private int _currentOrdinal = 0;
        private int _item = 1;
        private int _previous = 0;
        private int _next = 1;

        public FibonacciCalculator(ISeriesCache cache)  : base(cache)
        {
        }

        protected override bool ValidateOrdinal(int ordinal)
        {
            var baseResult = base.ValidateOrdinal(ordinal);

            return baseResult && ordinal > 0;
        }

        protected override int CalculateOrdinal (int ordinal)
        {
            while (_currentOrdinal < ordinal)
            {
                _next = _previous + _item;
                _previous = _item;
                _item = _next;

                _currentOrdinal++;
            }

            return _next;
        }
    }
}