﻿using System;

namespace Fibonacci.Core.Helpers
{
    public abstract class SeriesCalculator : NonCachingCalculator, ICachingSeriesCalculator
    {
        private readonly ISeriesCache _cache;

        protected SeriesCalculator(ISeriesCache cache)
        {
            _cache = cache;
        }

        public override int Calculate(int ordinal)
        {
            if (!ValidateOrdinal(ordinal))
            {
                throw new ArgumentOutOfRangeException(nameof(ordinal));
            }

            if (_cache.TryGetCacheItem(ordinal, out int result))
            {
                return result;
            }

            var value = base.Calculate(ordinal);

            _cache.CacheItem(ordinal, value);

            return value;
        }

        public int GetCachedItemsCount()
        {
            return _cache.GetNumberOfCachedItems();
        }
    }

    public abstract class NonCachingCalculator : ISeriesCalculator
    {
        protected abstract int CalculateOrdinal(int ordinal);

        protected virtual bool ValidateOrdinal(int ordinal)
        {
            return ordinal <= 8;
        }

        public virtual int Calculate(int ordinal)
        {
            if (!ValidateOrdinal(ordinal))
            {
                throw new ArgumentOutOfRangeException(nameof(ordinal));
            }

            return CalculateOrdinal(ordinal);
        }
    }
}