﻿namespace Fibonacci.Core.Helpers
{
    public interface ISeriesCalculator
    {
        int Calculate(int ordinal);
    }

    public interface ICachingSeriesCalculator : ISeriesCalculator, ICachingEntity
    {

    }

    public interface ICachingEntity
    {
        int GetCachedItemsCount();
    }
}